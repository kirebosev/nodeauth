var express = require('express');
var router = express.Router();
var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/register', function(req, res, next) {
  res.render('register', {
    'title': 'Register'
  }) 
});

router.get('/login', function(req, res, next) {
  res.render('login', {
    'title': 'Login'
  })
});

router.post('/register', function(req, res, next){
      // Get form values
      var name = req.body.name;
      var email = req.body.email;
      var username = req.body.username;
      var password = req.body.password;
      var password2 = req.body.password2; 
 
  // Form Validation
  req.checkBody('name', 'Name can not be empty').notEmpty();
  req.checkBody('email', 'Email can not be empty').notEmpty();
  req.checkBody('email', 'Name can not be empty').isEmail();
  req.checkBody('username', 'Username can not be empty').notEmpty();
  req.checkBody('password', 'Password can not be empty').notEmpty();
  req.checkBody('password2', 'Passwords dont mach').equals(req.body.password);
  

  // Check for errors
  var errors = req.validationErrors();

  if(errors) {
    res.render('register', {
      errors: errors,
      name: name,
      username: username,
      email: email,
      password: password,
      password2: password2
    });
  } else {
       var newUser = new User({
          name: name,
          username: username,
          email: email,
          password: password
   });  

  // Create User
  User.createUser(newUser, function (err, user) {
    if (err) throw err;
    console.log(user);
  });  

    req.flash('success', 'Now you can log in');
    res.location('/');
    res.redirect('/');
  }
});

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});

passport.use(new LocalStrategy(
  function(username, password, done) {
      User.getUserByUsername(username, function(err, user){
        if(err) throw err;
        if(!user) {
          console.log('Unknown User');
          return done(null, false, {message: 'Unknown User'});
        }
        User.comparePassword(password, user.password, function(err, isMatch){
          if(err) throw err;
          if(isMatch){
            return done(null, user);
          } else {
              console.log('Invalid Password');
              return done(null, false, {message:'invalid password'})
          }          
        })
      });
    }
  ));

router.post('/login', passport.authenticate('local', {failureRedirect:'/users/login'}),function(req, res){
  console.log('Authentication successful');
  req.flash('success', 'You are logged in');
  res.redirect('/');
});

router.get('/logout', function(req, res){
  req.logout();
  req.flash('success', 'You have been logged out');
  res.redirect('/users/login');
});



module.exports = router;
